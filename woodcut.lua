start=0
warned=false
toggler=true
rednet.open("left")
os.loadAPI("server")
sender="woodcutter"
problem=false
function checkFor(string, damage)
    for i=1,16 do
        local check = turtle.getItemDetail(i)
        if damage ~= nil then
            if check ~= nil and check.name == string and check.damage == damage then
                return i
            end
        else 
            if check ~= nil and check.name == string then
                return i
            end
        end
    end
    return -1
end
function firstFreeSlot()
    for i=1,16 do
        if turtle.getItemCount(i) == 0 then
            return i
        end
    end
    return -1
end
function suck(name, damage)
    if checkFor(name, damage) > 0 then
        turtle.select(checkFor(name, damage))
    end
    turtle.suck()
end
function log()
    update=server.createUpdate(sender, "Keine Probleme")
    server.sendUpdate(update)
    local success, inspect = turtle.inspect()
    if inspect.name == "minecraft:sapling" then
        if checkFor("minecraft:dye", 15) > 0 and toggler then
            turtle.select(checkFor("minecraft:dye", 15))
            turtle.place()
        else
            update=server.createUpdate(sender, "Kein Knochenmehl mehr")
            server.sendUpdate(update)
            problem=true
        end
        toggler = not toggler
        return
    end
    if turtle.getFuelLevel() < 10 then
        if checkFor("minecraft:coal") > 0 then
            turtle.select(checkFor("minecraft:coal"))
            turtle.refuel()
            print("Erfolgreich getankt: " .. turtle.getFuelLevel())
            update=server.createUpdate(sender, "Erfolgreich getankt: " .. turtle.getFuelLevel())
            server.sendUpdate(update)
            problem=false
            warned=false
        else 
            if not warned then
                print("Achtung, bitte Kohle nachfuellen")
                update=server.createUpdate(sender, "Kein Treibstoff mehr")
                server.sendUpdate(update)
                problem=true
                warned=true
            end
            return
        end
    end
    if inspect.name == "minecraft:log" then
        turtle.select(1)
        start=start+1
        turtle.dig()
        if turtle.detectUp() then
            turtle.digUp()
        end
        turtle.up()
        return log()
    else 
        for a=0,start do
            turtle.down()
        end
        if checkFor("minecraft:sapling", nil) > 0 then
            turtle.select(checkFor("minecraft:sapling", nil))
            turtle.place()
            problem=false
        else
            update=server.createUpdate(sender, "Keine Setzlinge mehr")
            server.sendUpdate(update)
            problem=true
        end
        turtle.turnLeft()
        suck("minecraft:dye", 15)
        turtle.turnLeft()
        for i=1,16 do
            tmp = turtle.getItemDetail(i)
            if tmp ~= nil and tmp.name ~= "minecraft:dye" and tmp.name ~= "minecraft:sapling" and tmp.name ~= "minecraft:coal" then
            turtle.select(i)
                turtle.drop()
            end
        end
        turtle.turnRight()
        turtle.turnRight()
        start=0
    end
end
while true do
    log()
end
