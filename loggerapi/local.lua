rednet.open("right")
rednet.host("loggerapi", "logserver")
monitor=peripheral.wrap("left")
lines={}
lines["woodcutter"]=2
while true do
    id,update,prot=rednet.receive("loggerapi",1)
    if update ~= nil then
        data=textutils.unserialize(update)
        if data == nil then
            print("error: 2 caused by " .. id)
        elseif data["sender"] == nil then
            print("error: 3 caused by " .. id)
        elseif data["message"] == nil then
            print("error: 4 caused by " .. id)
        else
            local line=lines[data["sender"]]
            local message=data["message"]
            monitor.clearLine(line)
            monitor.setCursorPos(1,line)
            monitor.write(message)
        end
    end
end