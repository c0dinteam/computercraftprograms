mainid=-1
function checkRednet()
    if not rednet.isOpen() then
        print("Enable rednet first")
        return -1
    else
        return rednet.lookup("loggerapi", "logserver")
    end
end
function sendUpdate(update)
    mainid=checkRednet()
    rednet.send(mainid, update, "loggerapi")
end
function createUpdate(sender, message, color)
    local data = {}
    data["sender"]=sender
    data["message"]=message
    data["color"]=color
    return textutils.serialize(data)
end
